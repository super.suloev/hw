#include "../includes/books_actions.h"


int get_books_num() {
    int counter = 0;
    Book_t *this_book_p;
    this_book_p = data_p->book_head_p;
    while (this_book_p != NULL) {
        ++counter;
        this_book_p = this_book_p->next_book_p;
    }
    return counter;
}


short book_in_db_and_full(long ISBN, Book_t *head_p) {
    Book_t *this_book_p;
    this_book_p = head_p;

    while (this_book_p != NULL) {
        if (this_book_p->ISBN == ISBN && this_book_p->available_books_number == this_book_p->books_number)
            return 1;
        this_book_p = this_book_p->next_book_p;
    }
    return 0;
}


void add_new_book() {
    long ISBN;
    printf("ISBN (10 digits): \t");
    scanf("%ld", &ISBN);
    if (book_in_db_and_full(ISBN, data_p->book_head_p))
        printf("The book with current ISBN already added\n\n");
    else {
        Book_t *new_book_p;
        new_book_p = (Book_t*) malloc(sizeof(Book_t));
        new_book_p->ISBN = ISBN;
        printf("Number of available books: \t");
        scanf("%d", &new_book_p->books_number);
        new_book_p->available_books_number = new_book_p->books_number;
        printf("Author: \t");
        scanf("%s", &new_book_p->author);
        printf("Label: \t");
        scanf("%s", &new_book_p->label);
        data_p->book_head_p = add_new_book_to_db(new_book_p, data_p->book_head_p);
        print_log("Book added successfully");
    }
}



void delete_by_ISBN() {
    long ISBN;
    printf("To delete enter ISBN (10 digits): \t");
    scanf("%ld", &ISBN);
    if (book_in_db_and_full(ISBN, data_p->book_head_p)) {
        delete_by_ISBN_from_db(ISBN, data_p->book_head_p);
        print_log("Book deleted successfully");
    }
    else
        printf("It is no book with %d ISBN \n\n", ISBN);
}

void view_book_info() {
    long ISBN;
    printf("Enter ISBN to show information: \t");
    scanf("%ld", &ISBN);
    Book_t *this_book_p;
    this_book_p = data_p->book_head_p;

    while (this_book_p != NULL) {
        if (this_book_p->ISBN == ISBN) {
            printf("Label: %s \n", this_book_p->label);
            printf("Author: %s \n", this_book_p->author);
            printf("Total number: %d \n", this_book_p->books_number);
            printf("Available number: %d \n\n", this_book_p->available_books_number);
            break;
        }
        this_book_p = this_book_p->next_book_p;
    }

    if (this_book_p == NULL)
        printf("Book with %d ISBN was not found\n\n", ISBN);
}


void view_all_books_info() {
    int books_num = get_books_num();
    Book_t *array[books_num];
    Book_t *this_book_p;
    this_book_p = data_p->book_head_p;
    for (int i = 0; i < books_num; ++i) {
        array[i] = this_book_p;
        this_book_p = this_book_p->next_book_p;
    }

    for (int i = 0; i < books_num - 1; ++i) {
        for (int j = i; j < books_num - 1; ++j) {
            if (array[j]->ISBN > array[j+1]->ISBN) {
                Book_t *temp_p;
                temp_p = array[j];
                array[j] = array[j+1];
                array[j+1] = temp_p;
            }
        }
    }

    for (int i = 0; i < books_num; ++i) {
        printf("%ld:\n", array[i]->ISBN);
        printf("Label: %s \n", array[i]->label);
        printf("Author: %s \n", array[i]->author);
        printf("Total number: %d \n", array[i]->books_number);
        printf("Available number: %d \n\n", array[i]->available_books_number);
    }
}


void edit_book_info() {
    long ISBN;
    printf("Enter ISBN to edit information about book: \t");
    scanf("%ld", &ISBN);

    Book_t *this_book_p;
    this_book_p = data_p->book_head_p;

    while (this_book_p != NULL) {
        if (this_book_p->ISBN == ISBN) {
            printf("New author: \t");
            scanf("%s", &this_book_p->author);
            printf("New Label: \t");
            scanf("%s", this_book_p->label);
            print_log("Book edited successfully");
            break;
        }
        this_book_p = this_book_p->next_book_p;
    }

    if (this_book_p == NULL)
        printf("Book with %ld ISBN was not found\n\n", ISBN);
}


void edit_book_number() {
    long ISBN;
    printf("Enter ISBN to edit books number: \t");
    scanf("%ld", &ISBN);

    Book_t *this_book_p;
    this_book_p = data_p->book_head_p;

    while (this_book_p != NULL) {
        if (this_book_p->ISBN == ISBN) {
            int new_num;
            printf("New number: \t");
            scanf("%d", &new_num);
            if (new_num < this_book_p->books_number - this_book_p->available_books_number) {
                printf("You can't change number to %d because students have %d book(s)\n\n", new_num,
                       this_book_p->books_number - this_book_p->available_books_number);
                break;
            }
            else {
                this_book_p->available_books_number = this_book_p->available_books_number += new_num - this_book_p->books_number;
                this_book_p->books_number = new_num;
                print_log("Book number changed successfully");
                break;
            }
        }
        this_book_p = this_book_p->next_book_p;
    }

    if (this_book_p == NULL)
        printf("Book with %ld ISBN was not found\n\n", ISBN);
}


void give_book() {
    long ISBN;
    printf("Enter ISBN to give book: \t");
    scanf("%ld", &ISBN);

    Book_t *this_book_p;
    this_book_p = data_p->book_head_p;

    while (this_book_p != NULL) {
        if (this_book_p->ISBN == ISBN) {
            if (this_book_p->available_books_number == 0) {
                printf("It is no available books\n\n");
            }
            else {
                char card_num[10];
                printf("Enter student card_num number: \t");
                scanf("%s", &card_num);
                Student_t *this_student_p;
                this_student_p = data_p->student_head_p;

                while (this_student_p != NULL) {
                    if (strcmp(this_student_p->card_num, card_num) == 0) {
                        --this_book_p->available_books_number;
                        Book_on_hands_t *new_book_on_hands_p;
                        new_book_on_hands_p = (Book_on_hands_t*) malloc(sizeof(Book_on_hands_t));
                        strcpy(new_book_on_hands_p->card_num, card_num);
                        new_book_on_hands_p->ISBN = ISBN;
                        time_t t = time(NULL);
                        struct tm *now = localtime(&t);
                        Date_t date;
                        date.day = now->tm_mday;
                        date.month = now->tm_mon + 1 + 1;
                        date.year = now->tm_year + 1900;
                        new_book_on_hands_p->date = date;
                        data_p->book_on_hand_head_p = add_new_book_on_hands_to_db(new_book_on_hands_p, data_p->book_on_hand_head_p);
                        print_log("Book gave successfully");
                        break;
                    }
                    this_student_p = this_student_p->next_student_p;
                }

                if (this_student_p == NULL)
                    printf("Student with %s card num was not found\n\n", card_num);
            }
            break;
        }
        this_book_p = this_book_p->next_book_p;
    }

    if (this_book_p == NULL)
        printf("Book with %ld ISBN was not found\n\n", ISBN);
}


void take_book() {
    long ISBN;
    printf("Enter ISBN to take book: \t");
    scanf("%ld", &ISBN);
    char card_num[10];
    printf("Enter student card_num number: \t");
    scanf("%s", &card_num);

    delete_book_on_hands_by_ISBN_from_db(ISBN, data_p->book_on_hand_head_p, card_num);
}
