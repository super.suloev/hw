#include "../includes/logs.h"

void print_log(char text[]) {
    FILE *fp;
    fp = fopen("../logs.txt", "a");

    time_t t = time(NULL);
    struct tm *now = localtime(&t);
    char str[20];
    strftime(str, sizeof(str), "%d.%m.%Y %T", now);
    fprintf(fp, "%s,%s,%s\n", str, data_p->active_user->login, text);
    fclose(fp);
}