#include "../includes/login.h"

void auth(Data_t *data_p) {
    while (data_p->active_user == NULL) {
        char login[MAX_STRING_SIZE];
        char password[MAX_STRING_SIZE];
        char pass_hash[HASH_SIZE];

        printf("Login:\t");
        scanf("%s", &login);
        printf("Password:\t");
        scanf("%s", &password);

        calculate_sha(pass_hash, password, strlen(password));

        User_t *this_user_p;
        this_user_p = data_p->user_head_p;

        while (this_user_p != NULL && data_p->active_user == NULL) {
            if (strcmp(login, this_user_p->login) == 0
            && strcmp(pass_hash, this_user_p->hash)) {
                data_p->active_user = this_user_p;
                print_log("Log in successfully");
            }
            this_user_p = this_user_p->next_user_p;
        }

        if (data_p->active_user == NULL)
            printf("Login or/and password wrong! Try again\n\n");
    }
}
