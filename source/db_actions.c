#include "../includes/db_actions.h"


Book_t *add_new_book_to_db(Book_t *new_book_p, Book_t *head_p) {
    // Adding to list (I think it is stack, but it is no differance, it works!!!!!)
    if (head_p == NULL) {
        new_book_p->next_book_p = NULL;
    }
    else {
        head_p->preview_book_p = new_book_p;
        new_book_p->next_book_p = head_p;
    }
    return new_book_p;
}


void delete_by_ISBN_from_db(long ISBN, Book_t *head_p) {
    Book_t *this_book_p;
    this_book_p = head_p;

    while (this_book_p != NULL) {
        if (this_book_p->ISBN == ISBN) {
            if (this_book_p->next_book_p == NULL) { // Last book
                if (this_book_p->preview_book_p != NULL) // If we have more than one book
                    this_book_p->preview_book_p->next_book_p = NULL;
                else // One book
                    data_p->book_head_p = NULL;
            }
            else if (this_book_p->preview_book_p == NULL) { // First book
                this_book_p->next_book_p->preview_book_p = NULL;
                data_p->book_head_p = this_book_p->next_book_p;
            }
            free(this_book_p);
            break;
        }
        this_book_p = this_book_p->next_book_p;
    }
}


Student_t *add_new_student_to_db(Student_t *new_student_p, Student_t *head_p) {
    if (head_p == NULL) {
        new_student_p->next_student_p = NULL;
    }
    else {
        head_p->preview_student_p = new_student_p;
        new_student_p->next_student_p = head_p;
    }
    return new_student_p;
}

void delete_student_from_db(char card_num[], Student_t *head_p) {
    Student_t *this_student_p;
    this_student_p = head_p;

    while (this_student_p != NULL) {
        if (strcmp(this_student_p->card_num, card_num) == 0) {
            if (this_student_p->next_student_p == NULL) { // Last
                if (this_student_p->preview_student_p != NULL) // If we have more than one
                    this_student_p->preview_student_p->next_student_p = NULL;
                else // One
                    data_p->student_head_p = NULL;
            }
            else if (this_student_p->preview_student_p == NULL) { // First book
                this_student_p->next_student_p->preview_student_p = NULL;
                data_p->student_head_p = this_student_p->next_student_p;
            }
            free(this_student_p);
            break;
        }
        this_student_p = this_student_p->next_student_p;
    }
}


Book_on_hands_t *add_new_book_on_hands_to_db(Book_on_hands_t *new_book_on_hands_p, Book_on_hands_t *head_p) {
    if (head_p == NULL) {
        new_book_on_hands_p->next_book_on_hands_p = NULL;
    }
    else {
        head_p->preview_book_on_hands_p = new_book_on_hands_p;
        new_book_on_hands_p->next_book_on_hands_p = head_p;
    }
    return new_book_on_hands_p;
}


void delete_book_on_hands_by_ISBN_from_db(long ISBN, Book_on_hands_t *head_p, char card_num[]) {
    Book_on_hands_t *this_book_on_hands_p;
    this_book_on_hands_p = head_p;

    while (this_book_on_hands_p != NULL) {
        if (this_book_on_hands_p->ISBN == ISBN && strcmp(this_book_on_hands_p->card_num, card_num) == 0) {
            if (this_book_on_hands_p->next_book_on_hands_p == NULL) { // Last
                if (this_book_on_hands_p->preview_book_on_hands_p != NULL) // More than one book
                    this_book_on_hands_p->preview_book_on_hands_p->next_book_on_hands_p = NULL;
                else // One book
                    data_p->book_on_hand_head_p = NULL;
            }
            else if (this_book_on_hands_p->preview_book_on_hands_p == NULL) { // First book
                this_book_on_hands_p->next_book_on_hands_p->preview_book_on_hands_p = NULL;
                data_p->book_on_hand_head_p = this_book_on_hands_p->next_book_on_hands_p;
            }
            free(this_book_on_hands_p);
            this_book_on_hands_p = head_p;

            Book_t *this_book_t;
            this_book_t = data_p->book_head_p;
            while (this_book_t != NULL) {
                if (this_book_t->ISBN == ISBN) {
                    ++this_book_t->available_books_number;
                    break;
                }
                this_book_t = this_book_t->next_book_p;
            }
            // So, I could in Book_on_hand_t use pointer to book instead ISBN
            // But I realized it too late
            print_log("Book took successfully");
            break;
        }
        this_book_on_hands_p = this_book_on_hands_p->next_book_on_hands_p;
    }

    if (this_book_on_hands_p == NULL)
        printf("Student %s haven't book with %dl ISBN\n\n", card_num, ISBN);
}
