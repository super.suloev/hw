#include "../includes/students_actions.h"

short student_in_db(char card_num[]) {
    Student_t *this_student;
    this_student = data_p->student_head_p;
    while (this_student != NULL) {
        if (strcmp(this_student->card_num, card_num) == 0)
            return 1;
        this_student = this_student->next_student_p;
    }
    return 0;
}

short student_have_books(char card_num[]) {
    Book_on_hands_t *item_p;
    item_p = data_p->book_on_hand_head_p;

    while (item_p != NULL) {
        if (strcmp(item_p->card_num, card_num) == 0)
            return 1;
        item_p = item_p->next_book_on_hands_p;
    }
    return 0;
}

void add_new_student() {
    char card_num[10];
    printf("Card number: \t");
    scanf("%s", &card_num);
    if (student_in_db(card_num)) {
        printf("Student with %s card number already added\n\n");
    }
    else {
        Student_t *new_student_p;
        new_student_p = (Student_t*) malloc(sizeof(Student_t));
        printf("Name: \t");
        scanf("%s", &new_student_p->first_name);
        printf("Family name: \t");
        scanf("%s", &new_student_p->last_name);
        printf("Specialty: \t");
        scanf("%s", &new_student_p->specialty);
        printf("Department: \t");
        scanf("%s", &new_student_p->department);
        strcpy(new_student_p->card_num, card_num);
        data_p->student_head_p = add_new_student_to_db(new_student_p, data_p->student_head_p);
        print_log("New student added successfully");
    }
}


void delete_student() {
    char card_num[10];
    printf("Enter the card number to delete student: \t");
    scanf("%s", &card_num);
    if (!student_have_books(card_num)) {
        delete_student_from_db(card_num, data_p->student_head_p);
        print_log("Student deleted successfully");
    }
    else
        printf("You can't delete student %s, he have book(s)\n\n");
}


void edit_student_info() {
    char card_num[10];
    printf("Enter the card number to edit info about student: \t");
    scanf("%s", &card_num);

    Student_t *this_student_p;
    this_student_p = data_p->student_head_p;

    while (this_student_p != NULL) {
        if (strcmp(this_student_p->card_num, card_num) == 0) {
            printf("Name: \t");
            scanf("%s", &this_student_p->first_name);
            printf("Family name: \t");
            scanf("%s", &this_student_p->last_name);
            printf("Department: \t");
            scanf("%s", &this_student_p->department);
            printf("Speciality: \t");
            scanf("%s", &this_student_p->specialty);
            print_log("Student edited successfully");
            break;
        }
        this_student_p = this_student_p->next_student_p;
    }

    if (this_student_p == NULL)
        printf("Student with %s card number was not found\n\n", card_num);
}

void print_student(Student_t *this_student_p) {
    printf("Name: \t %s\n", this_student_p->first_name);
    printf("Family name: \t %s\n", this_student_p->last_name);
    printf("Department: \t %s\n", this_student_p->department);
    printf("Speciality: \t %s\n\n", this_student_p->specialty);
}

void view_student_info() {
    char card_num[10];
    printf("Enter the card number to show info: \t");
    scanf("%s", &card_num);

    Student_t *this_student_p;
    this_student_p = data_p->student_head_p;

    while (this_student_p != NULL) {
        if (strcmp(this_student_p->card_num, card_num) == 0) {
            print_student(this_student_p);
            break;
        }
        this_student_p = this_student_p->next_student_p;
    }

    if (this_student_p == NULL)
        printf("Student %s was not found\n\n", card_num);
}

void search_by_family_name() {
    char last_name[50];
    printf("Enter the family name: \t");
    scanf("%s", &last_name);

    Student_t *this_student_p;
    this_student_p = data_p->student_head_p;
    short was_found = 0;

    while (this_student_p != NULL) {
        if (strcmp(this_student_p->last_name, last_name) == 0) {
            print_student(this_student_p);
            was_found = 1;
        }
        this_student_p = this_student_p->next_student_p;
    }

    if (!was_found)
        printf("Students with %s family name were not found\n\n", last_name);
}
