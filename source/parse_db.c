#include "../includes/parse_db.h"

Data_t *data_p;

void parse_db() {
    data_p = (Data_t*) malloc(sizeof(Data_t) + 8);
    data_p->active_user = NULL;
    data_p->book_head_p = init_library();
    data_p->student_head_p = init_students();
    data_p->user_head_p = init_administration();
    data_p->book_on_hand_head_p = init_books_on_hands();
}


Book_t *init_library() {
    Book_t *head_p;
    head_p = NULL;
    FILE *fp;
    fp = fopen("../data_base/books.csv", "r");

    char row[MAX_STRING_SIZE];
    while (!feof(fp)) {
        Book_t *new_book_p;
        new_book_p = (Book_t*) malloc(sizeof(Book_t));
        new_book_p->preview_book_p = NULL;

        fgets(row, MAX_STRING_SIZE, fp);
        char *substring;
        substring = strtok(row, ","); // ISBN
        new_book_p->ISBN = atol(substring);

        substring = strtok(NULL, ","); // Author
        strcpy(new_book_p->author, substring);

        substring = strtok(NULL, ","); // Label
        strcpy(new_book_p->label, substring);

        substring = strtok(NULL, ","); // Number of books
        new_book_p->books_number = atoi(substring);

        substring = strtok(NULL, "\n"); // Number of available books
        new_book_p->available_books_number = atoi(substring);

        head_p = add_new_book_to_db(new_book_p, head_p);
    }
    fclose(fp);
    return head_p;
}


Student_t *init_students() {
    Student_t *head_p;
    head_p = NULL;
    FILE *fp;
    fp = fopen("../data_base/students.csv", "r");

    char row[MAX_STRING_SIZE];
    while (!feof(fp)) {
        Student_t *new_student_p;
        new_student_p = (Student_t*) malloc(sizeof(Student_t));
        new_student_p->preview_student_p = NULL;

        fgets(row, MAX_STRING_SIZE, fp);
        char *substring;
        substring = strtok(row, ","); // card number
        strcpy(new_student_p->card_num, substring);

        substring = strtok(NULL, ","); // first name
        strcpy(new_student_p->first_name, substring);

        substring = strtok(NULL, ","); // last name
        strcpy(new_student_p->last_name, substring);

        substring = strtok(NULL, ","); // department
        strcpy(new_student_p->department, substring);

        substring = strtok(NULL, "\n"); // specialty
        strcpy(new_student_p->specialty, substring);

        // Kirill, do you check all our codes?
        head_p = add_new_student_to_db(new_student_p, head_p);

    }

    fclose(fp);
    return head_p;
}


User_t *init_administration() {
    User_t *head_p;
    head_p = NULL;
    FILE *fp;
    fp = fopen("../data_base/users.csv", "r");

    char row[MAX_STRING_SIZE];
    while (!feof(fp)) {
        User_t *new_user_p;
        new_user_p = (User_t*) malloc(sizeof(User_t));

        fgets(row, MAX_STRING_SIZE, fp);
        char *substring;
        substring = strtok(row, ","); // login
        strcpy(new_user_p->login, substring);

        substring = strtok(NULL, ","); // hash
        strcpy(new_user_p->hash, substring);

        substring = strtok(NULL, ","); // books actions right
        new_user_p->books_actions_right = atoi(substring);

        substring = strtok(NULL, "\n"); // students actions right
        new_user_p->students_actions_right = atoi(substring);

        if (head_p == NULL)
            new_user_p->next_user_p = NULL;
        else
            new_user_p->next_user_p = head_p;
        head_p = new_user_p;
    }

    fclose(fp);
    return head_p;
}

Book_on_hands_t *init_books_on_hands() {
    Book_on_hands_t *head_p;
    head_p = NULL;
    FILE *fp;
    fp = fopen("../data_base/student_books.csv", "r");

    char row[MAX_STRING_SIZE];
    while (!feof(fp)) {
        Book_on_hands_t *new_book_on_hands_p;
        new_book_on_hands_p = (Book_on_hands_t*) malloc(sizeof(Book_on_hands_t));
        new_book_on_hands_p->preview_book_on_hands_p = NULL;

        fgets(row, MAX_STRING_SIZE, fp);
        char *substring;
        substring = strtok(row, ","); // ISBN
        new_book_on_hands_p->ISBN = atol(substring);

        substring = strtok(NULL, ","); // card number
        strcpy(new_book_on_hands_p->card_num, substring);

        Date_t date; // date
        substring = strtok(NULL, "."); // day
        date.day = atoi(substring);
        substring = strtok(NULL, "."); // month
        date.month = atoi(substring);
        substring = strtok(NULL, "\n"); // year
        date.year = atoi(substring);
        new_book_on_hands_p->date = date;

        head_p = add_new_book_on_hands_to_db(new_book_on_hands_p, head_p);
    }

    fclose(fp);
    return head_p;
}
