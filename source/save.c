#include "../includes/save.h"

void save_session() {
    FILE *fp;
    fp = fopen("../data_base/students.csv", "w");
    Student_t *this_student_p;
    this_student_p = data_p->student_head_p;

    while (this_student_p != NULL) {
        fprintf(fp, "%s,%s,%s,%s,%s", this_student_p->card_num, this_student_p->first_name,
                this_student_p->last_name, this_student_p->department, this_student_p->specialty);
        if (this_student_p->next_student_p != NULL) { // Not last student
            this_student_p = this_student_p->next_student_p;
            free(this_student_p->preview_student_p);
            fprintf(fp, "\n");
        }
        else {
            free(this_student_p);
            this_student_p = NULL;
        }
    }
    fclose(fp);

    fp = fopen("../data_base/books.csv", "w");
    Book_t *this_book_p;
    this_book_p = data_p->book_head_p;

    while (this_book_p != NULL) {
        fprintf(fp, "%ld,%s,%s,%d,%d", this_book_p->ISBN, this_book_p->author,
                this_book_p->label, this_book_p->books_number, this_book_p->available_books_number);
        if (this_book_p->next_book_p != NULL) { // Not last book
            this_book_p = this_book_p->next_book_p;
            free(this_book_p->preview_book_p);
            fprintf(fp, "\n");
        }
        else {
            free(this_book_p);
            this_book_p = NULL;
        }
    }
    fclose(fp);

    fp = fopen("../data_base/student_books.csv", "w");
    Book_on_hands_t *this_book_on_hands;
    this_book_on_hands = data_p->book_on_hand_head_p;

    while (this_book_on_hands != NULL) {
        fprintf(fp, "%ld,%s,%d.%d.%d", this_book_on_hands->ISBN, this_book_on_hands->card_num,
                this_book_on_hands->date.day, this_book_on_hands->date.month, this_book_on_hands->date.year);
        if (this_book_on_hands->next_book_on_hands_p != NULL) { // Not last book on hands
            this_book_on_hands = this_book_on_hands->next_book_on_hands_p;
            free(this_book_on_hands->preview_book_on_hands_p);
            fprintf(fp, "\n");
        }
        else {
            free(this_book_on_hands);
            this_book_on_hands = NULL;
        }
    }
    fclose(fp);
    print_log("Session saved successfully");
}