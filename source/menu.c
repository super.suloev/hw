#include "../includes/menu.h"

short menu_num;

_Noreturn void loop_menu() {
    if (data_p->active_user->books_actions_right + data_p->active_user->students_actions_right == 2)
        menu_num = CHOOSE_MENU;
    else if (data_p->active_user->books_actions_right)
        menu_num = BOOKS_MENU;
    else
        menu_num = STUDENTS_MENU;


    while (1) {
        switch (menu_num) {
            case CHOOSE_MENU:
                show_global_menu();
                break;

            case BOOKS_MENU:
                show_books_menu();
                break;

            case STUDENTS_MENU:
                show_students_menu();
                break;
        }
    }
}

void show_global_menu() {
    printf("° ˛ ° ˚* _Π_____*☽*˚ ˛     \t\t 1 - Books menu\n");
    printf("✩ ˚˛˚*/______/__＼。✩˚ ˚˛   \t 2 - Students menu\n");
    printf("˚ ˛˚˛˚｜ 田田 ｜ 門 ｜ ˚ ˚     \t 0 - Exit\n");
    printf("\t\t\t\t\t\t\t\t");
    char choice;
    scanf("%s", &choice);

    switch (choice) {
        case '1':
            menu_num = 1;
            break;

        case '2':
            menu_num = 2;
            break;

        case '0':
            save_session();
            exit(0);
            break;

        default:
            printf("Invalid input\n\n");
    }
}


void show_books_menu() {
    printf("      ______ ______ \t \t1 - Add new book\n");
    printf("    _/      Y      \\_ \t\t2 - Delete by ISBN\n");
    printf("   // ~~ ~~ | ~~ ~  \\\\     \t3 - View info about book\n");
    printf("  // ~ ~ ~~ | ~~~ ~~ \\\\ \t4 - View info about all books\n");
    printf(" //________.|.________\\\\ \t5 - Edit book info\n");
    printf("`----------`-'----------' \t6 - Edit book number\n");
    printf("\t\t\t\t\t\t\t7 - Give book\n");
    printf("\t\t\t\t\t\t\t8 - Take book\n");
    printf("\t\t\t\t\t\t\t0 - Exit\n");
    printf("\t\t\t\t\t\t\t");
    char choice;
    scanf("%s", &choice);

    switch (choice) {
        case '1':
            print_log("addBook");
            add_new_book();
            break;

        case '2':
            print_log("deleteBook");
            delete_by_ISBN();
            break;

        case '3':
            print_log("viewBookInfo");
            view_book_info();
            break;

        case '4':
            print_log("viewAllBooksInfo");
            view_all_books_info();
            break;

        case '5':
            print_log("editBookInfo");
            edit_book_info();
            break;

        case '6':
            print_log("editBookNumber");
            edit_book_number();
            break;

        case '7':
            print_log("giveBook");
            give_book();
            break;

        case '8':
            print_log("takeBook");
            take_book();
            break;

        case '0':
            if (data_p->active_user->students_actions_right)
                menu_num = CHOOSE_MENU;
            else {
                save_session();
                exit(0);
            }
            break;

        default:
            printf("Invalid input\n\n");
    }
}


void show_students_menu() {
    printf("    _____ \t\t 1 - Add new student\n");
    printf("   /    _\\__  \t 2 - Delete student\n");
    printf(" /|    /    \\ \t 3 - Edit info about student\n");
    printf("| |   |      | \t 4 - View info\n");
    printf("| |    \\ __ / \t 5 - Search by family name\n");
    printf("| |   _   | \t 0 - Exit\n");
    printf(" \\|  | |  |\n");
    printf("   \\/   \\/ \t \t");
    char choice;
    scanf("%s", &choice);

    switch (choice) {
        case '1':
            print_log("addStudent");
            add_new_student();
            break;

        case '2':
            print_log("deleteStudent");
            delete_student();
            break;

        case '3':
            print_log("editStudent");
            edit_student_info();
            break;

        case '4':
            print_log("viewStudentInfo");
            view_student_info();
            break;

        case '5':
            print_log("searchStudentsByFamilyName");
            search_by_family_name();
            break;

        case '0':
            if (data_p->active_user->books_actions_right)
                menu_num = CHOOSE_MENU;
            else {
                save_session();
                exit(0);
            }
            break;

        default:
            printf("Invalid input\n\n");
    }
}