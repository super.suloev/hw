#ifndef GREATESTHOMEWORK_DATA_H
#define GREATESTHOMEWORK_DATA_H

#define MAX_STRING_SIZE 200
#define HASH_SIZE 64

typedef struct {
    int year, month, day;
} Date_t;


typedef struct Book_t {
    int books_number, available_books_number;
    long ISBN;
    char author[30], label[70];
    struct Book_t *next_book_p, *preview_book_p;
} Book_t;


typedef struct Student_t {
    char card_num[10], first_name[50], last_name[50], department[50], specialty[50];
    struct Student_t *next_student_p, *preview_student_p;
} Student_t;


typedef struct User_t {
    char login[60], hash[HASH_SIZE];
    short books_actions_right, students_actions_right;
    struct User_t *next_user_p;
} User_t;


typedef struct Book_on_hands_t {
    long ISBN;
    char card_num[10];
    Date_t date;
    struct Book_on_hands_t *next_book_on_hands_p, *preview_book_on_hands_p;
} Book_on_hands_t;


typedef struct {
    struct Book_t *book_head_p;
    struct Student_t *student_head_p;
    struct User_t *user_head_p;
    struct Book_on_hands_t *book_on_hand_head_p;
    struct User_t *active_user;
} Data_t;

extern short menu_num; // 0 - global; 1 - books; 2 - students
extern Data_t *data_p;


#endif
