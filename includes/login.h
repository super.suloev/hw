#ifndef GREATESTHOMEWORK_LOGIN_H
#define GREATESTHOMEWORK_LOGIN_H

#define STR_MAX_LENGTH 100

#include <string.h>
#include <stdio.h>
#include "data.h"
#include "sha256.h"
#include "logs.h"

void auth(Data_t *data_p);

#endif
