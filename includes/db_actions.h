#ifndef GREATESTHOMEWORK_DB_ACTIONS_H
#define GREATESTHOMEWORK_DB_ACTIONS_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "data.h"
#include "logs.h"

Book_t *add_new_book_to_db(Book_t *new_book_p, Book_t *head_p);
void delete_by_ISBN_from_db(long ISBN, Book_t *head_p);

Student_t *add_new_student_to_db(Student_t *new_student_p, Student_t *head_p);
void delete_student_from_db(char card_num[], Student_t *head_p);

Book_on_hands_t *add_new_book_on_hands_to_db(Book_on_hands_t *new_book_on_hands_p, Book_on_hands_t *head_p);
void delete_book_on_hands_by_ISBN_from_db(long ISBN, Book_on_hands_t *head_p, char card_num[]);


#endif
