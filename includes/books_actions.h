#ifndef GREATESTHOMEWORK_BOOKS_ACTIONS_H
#define GREATESTHOMEWORK_BOOKS_ACTIONS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "data.h"
#include "db_actions.h"
#include "logs.h"

int get_books_num();
short book_in_db_and_full(long ISBN, Book_t *head_p);
void add_new_book();
void delete_by_ISBN();
void view_book_info(); // Добавить функционала по ТЗ
void view_all_books_info();
void edit_book_info();
void edit_book_number();
void give_book();
void take_book();

#endif
