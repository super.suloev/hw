#ifndef GREATESTHOMEWORK_MENU_H
#define GREATESTHOMEWORK_MENU_H

#include <stdio.h>
#include <stdlib.h>
#include "data.h"
#include "books_actions.h"
#include "students_actions.h"
#include "save.h"
#include "logs.h"

enum {
    CHOOSE_MENU = 0,
    BOOKS_MENU,
    STUDENTS_MENU
};

_Noreturn void loop_menu();
void show_global_menu();
void show_books_menu();
void show_students_menu();


#endif
