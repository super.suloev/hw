#ifndef GREATESTHOMEWORK_STUDENTS_ACTIONS_H
#define GREATESTHOMEWORK_STUDENTS_ACTIONS_H

#include <stdio.h>
#include <stdlib.h>
#include "data.h"
#include "db_actions.h"

short student_in_db(char card_num[]);
short student_have_books(char card_num[]);
void add_new_student();
void delete_student();
void edit_student_info();
void print_student(Student_t *this_student_p);
void view_student_info();
void search_by_family_name();

#endif
