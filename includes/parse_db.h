#ifndef GREATESTHOMEWORK_PARSE_DB_H
#define GREATESTHOMEWORK_PARSE_DB_H


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "data.h"
#include "db_actions.h"

void parse_db();
Book_t *init_library(); // returns first book on linked array
Student_t *init_students(); // returns first student on linked array
User_t *init_administration(); // returns first user on linked array
Book_on_hands_t *init_books_on_hands(); // returns first information about book that has somebody on linked array


#endif
